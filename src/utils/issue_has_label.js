export default (issue, searchLabel) =>
  issue.labels.nodes.find(({ title }) => title == searchLabel) ? true : false;
