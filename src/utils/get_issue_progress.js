import getWorkflowLabel from "@/utils/get_workflow_label";

export default (issue) => {

  let workflowLabel = getWorkflowLabel(issue);
  if (issue.state === 'closed') {
    workflowLabel = 'closed';
  }

  const workflowProgressMap = {
    'design': 0,
    'blocked': 0,
    'planning breakdown': 10,
    'ready for dev': 20,
    'in dev': 30,
    'ready for review': 79,
    'in review': 80,
    'verification': 90,
    'production': 99,
    'closed': 100
  }

  return workflowProgressMap[workflowLabel] || 0;
}
