export default (issue) => {

  let label = issue.labels.nodes.find(({ title }) => title.match(/^group::/))?.title ||
    "";
  label = label.replace(/group::/, "");
  label = label.replace(/composition analysis/, "CA");
  label = label.replace(/static analysis/, "SAST");
  label = label.replace(/dynamic analysis/, "DAST");

  return label;
}
