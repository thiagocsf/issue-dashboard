import Vue from "vue";
import VueRouter from "vue-router";
import Team from "@/views/team.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/:milestone",
    name: "Team",
    component: Team,
  },
  {
    path: "/:milestone/:teamMembers",
    name: "TeamWithList",
    component: Team,
  }
];

const router = new VueRouter({
  routes,
});

export default router;
